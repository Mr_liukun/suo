package main

import (
	"github.com/astaxie/beego"
	_ "suo/routers"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"suo/models/login"
)

func init() {

	orm.RegisterDriver("mysql", orm.DRMySQL) //注册驱动
	orm.RegisterModel(new(login.User))//注册 model

	// 注册自己使用的数据库
	//orm.RegisterDataBase("qwe", "mysql", "root:123@tcp(localhost:3306)/onedb?charset=utf8", 30, 30)

	// 注册默认数据库

	orm.RegisterDataBase("default", "mysql", "root:123@tcp(localhost:3306)/suo?charset=utf8", 30, 30)


	//// 拦截未登陆的任何操作，强制跳转登录页面
	//var FilterUser = func(ctx *context.Context) {
	//	ok := ctx.Input.Session("name")
	//	if ok == nil && ctx.Request.RequestURI != "/login" {
	//		ctx.Redirect(302, "/login")
	//	}
	//}
	//
	//beego.InsertFilter("/*",beego.BeforeRouter,FilterUser)
}

func main() {
	beego.Run()
}

