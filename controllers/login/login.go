package login

import (
	"github.com/astaxie/beego"
	"suo/models/login"
	"github.com/astaxie/beego/orm"
)

type LoginController struct {
	beego.Controller
}


// 注册
func (this *LoginController) register() {



}



// 登陆
func (this *LoginController) Login() {

	username := this.GetString("username")
	password := this.GetString("password")

	if login.Login(username, password) {
		this.SetSession(username, username)
		this.Data["json"] = "登陆成功"
	}else {
		this.Data["json"] = "账号密码有误"
	}
}

// 通过用户名查找用户
func (this *LoginController) FindByUsername() {

	username := this.GetString("username")

	u := login.User{Username:username}
	if err := orm.NewOrm().Read(&u); err == nil {
		this.Data["json"] = u
	}else {
		this.Data["json"] = "查无此人"
	}
	this.ServeJSON()
}