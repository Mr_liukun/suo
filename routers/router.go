package routers

import (
	"suo/controllers"
	"github.com/astaxie/beego"
	"suo/controllers/login"
)

func init() {
    beego.Router("/", &controllers.MainController{})
    beego.Router("/index", &login.LoginController{}, "post:Login")
    beego.Router("/find", &login.LoginController{}, "get:FindByUsername")

}
