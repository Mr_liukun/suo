package login

import (
	"github.com/astaxie/beego/orm"
)

type User struct {
	Id int
	Username string
	Password string

}

func Login(username, password string) bool {

	u := User{Username:username, Password:password}

	//va := validation.Validation{}
	//
	//// 不能为空
	//va.Required(username, "username")
	//va.Required(password, "password")
	//// 最大长度限制
	//va.MaxSize(username, 8, "usernameMax")
	//va.MaxSize(password, 8, "usernameMax")

	//if va.HasErrors() {
	//	for _, err := range va.Errors {
	//		log.Print(err.Key, err.Message)
	//	}
	//	return false
	//}else
	if err := orm.NewOrm().Read(&u); err == nil{
		return true
	}
	return false


}


func Register(username, password, newpassword string) bool {

	u := User{Username:username}


	if password != newpassword {
		return false // 两次密码不一样

	}else if err := orm.NewOrm().Read(&u); err == nil && &u == nil {
		return false // 用户已存在
	}else {
		return true //成功注册
	}

}


/*

 */
func FindByUsername(username string) *User {


	u := User{Username:username}

	orm.NewOrm().Read(&u)

	return &u

}
